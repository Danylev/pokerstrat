### What is this repository for? ###
Simple solution for The Psychic Poker Player.
https://docs.google.com/document/d/1TU-udhx_VTGTgsV0udRxz8Fq8Z3H_1QC3MkhfbBaCgc/edit

More details you can find by executing "python solution.py -h"

!Execution with --mp(multiprocessing) flag, yield result in form of chunks!

### How do I get set up? ###
You need a python 3 for it.
https://www.python.org/downloads/

Also, you need a numpy:
Just invoke "pip install numpy", after installing python.